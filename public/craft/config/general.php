<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

return array(
  '*' => array(
  ),
  'craft' => array(
    'siteUrl'    => 'http://craft:8888/',
    'devMode'    => true
  ),
  'mark-one' => array(
    'siteUrl'    => 'http://localhost:8888/',
    'devMode'    => true
  ),
  'mark-one' => array(
    'siteUrl'    => 'http://mark-one:8888/',
    'devMode'    => true
  ),
  'mark1-new.beresponsive.co.za' => array(
    'siteUrl'    => 'http://mark1-new.beresponsive.co.za/'
  ),
  'mark1.co.za' => array(
    'siteUrl'    => 'https://mark1.co.za/'
  ),
);
