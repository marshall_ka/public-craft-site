# DemoUrls plugin for Craft CMS

Display encrypted urls for demo content

![Screenshot](resources/screenshots/plugin_logo.png)

## Installation

To install DemoUrls, follow these steps:

1. Download & unzip the file and place the `demourls` directory into your `craft/plugins` directory
2.  -OR- do a `git clone ???` directly into your `craft/plugins` folder.  You can then update it with `git pull`
3.  -OR- install with Composer via `composer require /demourls`
4. Install plugin in the Craft Control Panel under Settings > Plugins
5. The plugin folder should be named `demourls` for Craft to see it.  GitHub recently started appending `-master` (the branch name) to the name of the folder for zip file downloads.

DemoUrls works on Craft 2.4.x and Craft 2.5.x.

## DemoUrls Overview

-Insert text here-

## Configuring DemoUrls

-Insert text here-

## Using DemoUrls

-Insert text here-

## DemoUrls Roadmap

Some things to do, and ideas for potential features:

* Release it

Brought to you by [responsive](www.responsive.co.za)
