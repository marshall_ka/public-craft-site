<?php
/**
 * DemoUrls plugin for Craft CMS
 *
 * DemoUrls_VideoDemo Model
 *
 * --snip--
 * Models are containers for data. Just about every time information is passed between services, controllers, and
 * templates in Craft, it’s passed via a model.
 *
 * https://craftcms.com/docs/plugins/models
 * --snip--
 *
 * @author    responsive
 * @copyright Copyright (c) 2017 responsive
 * @link      www.responsive.co.za
 * @package   DemoUrls
 * @since     1.0.0
 */

namespace Craft;

class DemoUrls_VideoDemoModel extends BaseModel
{
    /**
     * Defines this model's attributes.
     *
     * @return array
     */
    protected function defineAttributes()
    {
        return array_merge(parent::defineAttributes(), array(
            'url'     => array(AttributeType::String, 'default' => 'http://example.com'),
        ));
    }

}