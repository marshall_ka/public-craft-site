<?php
/**
 * DemoUrls plugin for Craft CMS
 *
 * DemoUrls Variable
 *
 * --snip--
 * Craft allows plugins to provide their own template variables, accessible from the {{ craft }} global variable
 * (e.g. {{ craft.pluginName }}).
 *
 * https://craftcms.com/docs/plugins/variables
 * --snip--
 *
 * @author    responsive
 * @copyright Copyright (c) 2017 responsive
 * @link      www.responsive.co.za
 * @package   DemoUrls
 * @since     1.0.0
 */

namespace Craft;

class DemoUrlsVariable
{
    /**
     * Whatever you want to output to a Twig tempate can go into a Variable method. You can have as many variable
     * functions as you want.  From any Twig template, call it like this:
     *
     *     {{ craft.demoUrls.exampleVariable }}
     *
     * Or, if your variable requires input from Twig:
     *
     *     {{ craft.demoUrls.exampleVariable(twigValue) }}
     */
    public function exampleVariable($optional = null)
    {
        return "And away we go to the Twig template...";
    }
    
    
    /**
     * Whatever you want to output to a Twig tempate can go into a Variable method. You can have as many variable
     * functions as you want.  From any Twig template, call it like this:
     *
     *     {{ craft.demoUrls.getCeltraContent }}
     *
     * Or, if your variable requires input from Twig:
     *
     *     {{ craft.demoUrls.getCeltraContent(url) }}
     */
    public function getCeltraContent($url, $title)
    {
    
        $url = $url.'/frame';
        $ch = curl_init();
        $timeout = 5;
        $cookie_data =
          implode(
            "; ",
            array_map(
              function($k, $v) {
                  return "$k=$v";
              },
              array_keys($_COOKIE),
              array_values($_COOKIE)
            )
          );
        curl_setopt($ch, CURLOPT_COOKIE, $cookie_data);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); //safety first
        $data = curl_exec($ch);
        curl_close($ch);
        $strip_tags = array("meta"); //Strip everything we do not want.
        $data = str_replace('../../../','http://mark1.celtra.com/', $data);
        if(isset($strip_tags)){
            foreach ($strip_tags as $tag) {
                $data = preg_replace("/<\/?" . $tag . "(.|\s)*?>/","",$data);
            }
        }
        echo '<span class="demo-title">' . $title . '</span>';
        echo '<div class="demo_frame>'.$data.'</div>';
    }
    
    
    
}