<?php
/**
 * DemoUrls plugin for Craft CMS
 *
 * DemoUrls Translation
 *
 * @author    responsive
 * @copyright Copyright (c) 2017 responsive
 * @link      www.responsive.co.za
 * @package   DemoUrls
 * @since     1.0.0
 */

return array(
    'Translate me' => 'To this',
);
