<?php
/**
 * DemoUrls plugin for Craft CMS
 *
 * DemoUrls_VideoDemo Service
 *
 * --snip--
 * All of your plugin’s business logic should go in services, including saving data, retrieving data, etc. They
 * provide APIs that your controllers, template variables, and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 * --snip--
 *
 * @author    responsive
 * @copyright Copyright (c) 2017 responsive
 * @link      www.responsive.co.za
 * @package   DemoUrls
 * @since     1.0.0
 */

namespace Craft;

class DemoUrls_VideoDemoService extends BaseApplicationComponent
{
    /**
     * This function can literally be anything you want, and you can have as many service functions as you want
     *
     * From any other plugin file, call it like this:
     *
     *     craft()->demoUrls_videoDemo->exampleService()
     */
    public function exampleService()
    {
    }

}