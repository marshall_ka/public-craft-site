<?php
namespace Craft;

class ContactFormModel extends BaseModel
{
    protected function defineAttributes()
    {
        return array(
          'checkProgrammatic' => array(
            AttributeType::String,
            'label' => 'Programmatic'
          ),
          'checkPremium' => array(
            AttributeType::String,
            'label' => 'Premium'
          ),
          'checkMedia' => array(
            AttributeType::String,
            'label' => 'Media'
          ),
          'checkPpc' => array(
            AttributeType::String,
            'label' => 'Pay-Per-Click'
          ),
          'checkDesign' => array(
            AttributeType::String,
            'label' => 'Design'
          ),
          'checkData' => array(
            AttributeType::String,
            'label' => 'Data Management'
          ),
          'fromName' => array(
            AttributeType::String,
            'required' => true,
            'label' => 'Name'
          ),
          'fromSurname' => array(
            AttributeType::String,
            'label' => 'Your Surname'
          ),
          'contactNumber' => array(
            AttributeType::String,
            'label' => 'Your Contact Number'
          ),
          'fromEmail' => array(
            AttributeType::Email,
            'required' => true,
            'label' => 'Email'
          ),
          'message' => array(
            AttributeType::String,
            'required' => true,
            'label' => 'Message'
          ),
          'htmlMessage' => array(
            AttributeType::String
          ),
          'messageFields' => array(
            AttributeType::Mixed
          ),
          'subject' => array(
            AttributeType::String,
            'label' => 'Subject'
          ),
          'attachment' => AttributeType::Mixed,
        );
    }
}
