<?php
namespace Craft;

/**
 * Contact Form controller
 */
class ContactFormController extends BaseController
{
    /**
     * @var Allows anonymous access to this controller's actions.
     * @access protected
     */
    protected $allowAnonymous = true;
    
    
    /**
     * Sends an email based on the posted params.
     *
     * @throws Exception
     */
    public function actionSendMessage()
    {
        // prepare the posted form data
        $this->requirePostRequest();
        
        // get the form settings
        $settings = craft()->plugins->getPlugin('contactform')->getSettings();
        
        // get all the form post values
        $message = new ContactFormModel();
        $savedBody = false;
        $message->checkProgrammatic = craft()->request->getPost('checkProgrammatic');
        $message->checkPremium = craft()->request->getPost('checkPremium');
        $message->checkMedia = craft()->request->getPost('checkMedia');
        $message->checkPpc = craft()->request->getPost('checkPpc');
        $message->checkDesign = craft()->request->getPost('checkDesign');
        $message->checkData = craft()->request->getPost('checkDataManagement');
        $message->fromName = craft()->request->getPost('fromName');
        $message->fromSurname = craft()->request->getPost('fromSurname');
        $message->fromEmail = craft()->request->getPost('fromEmail');
        $message->contactNumber = craft()->request->getPost('contactNumber');
        $message->message = craft()->request->getPost('message');
        
        // prepare to save the values to the entry / cms / database
        $entry = new EntryModel();
        $section = craft()->sections->getSectionByHandle('contactFormEntries');
        $entry->sectionId = $section->id;
        $entry->typeId = 10;
        $entry->authorId = 1;
        $entry->enabled = true;
        $entry->getContent()->setAttributes(array(
          'title' => $message->fromName . ' - ' . $message->fromSurname,
          'contactName' => $message->fromName,
          'contactSurname' => $message->fromSurname,
          'contactNumber' => $message->contactNumber,
          'contactEmail' => $message->fromEmail,
          'contactMessage' => $message->message,
          'programmatic' => $message->checkProgrammatic,
          'premium' => $message->checkPremium,
          'media' => $message->checkMedia,
          'payperclick' => $message->checkPpc,
          'design' => $message->checkDesign,
          'datamanagement' => $message->checkData,
        ));
        
        // save the actual data into craft
        craft()->entries->saveEntry($entry);
    
        
        // get the form global settings
        $contactFormGlobals = craft()->globals->getSetByHandle('contactFormSettings');
    
        /**
         * Send email to enquirer
         */
        $email = new EmailModel();
        $email->toEmail = $message->fromEmail;
        $email->fromEmail = $contactFormGlobals->contactAdminEmail;
        $email->subject = $contactFormGlobals->contactReplySubject;
        $email->body .= '<p>' . $message->fromName . ' ' . $message->fromSurname . '</p>';
        $email->body .= '<p>' . $contactFormGlobals->contactReplyMessage . '</p>';
    
        craft()->email->sendEmail($email);
    
        // Set the message body
        $postedMessage = craft()->request->getPost('message');
        
        // Before compile event
        Craft::import('plugins.contactform.events.ContactFormMessageEvent');
        $event = new ContactFormMessageEvent($this, array(
          'postedMessage' => $postedMessage
        ));
        craft()->contactForm->onBeforeMessageCompile($event);
        
        if ($event->message && $event->messageFields) {
            $message->message = $event->message;
            $message->messageFields = $event->messageFields;
            
            if (!empty($event->htmlMessage)) {
                $message->htmlMessage = $event->htmlMessage;
            }
        } elseif ($postedMessage) {
            if (is_array($postedMessage)) {
                // Capture all of the message fields on the model in case there's a validation error
                $message->messageFields = $postedMessage;
                
                // Capture the original message body
                if (isset($postedMessage['body'])) {
                    // Save the message body in case we need to reassign it in the event there's a validation error
                    $savedBody = $postedMessage['body'];
                }
                
                // If it's false, then there was no messages[body] input submitted.  If it's '', then validation needs to fail.
                if ($savedBody === false || $savedBody !== '') {
                    // Compile the message from each of the individual values
                    $compiledMessage = '';
                    
                    foreach ($postedMessage as $key => $value) {
                        if ($key != 'body') {
                            if ($compiledMessage) {
                                $compiledMessage .= "\n\n";
                            }
                            
                            $compiledMessage .= $key . ': ';
                            
                            if (is_array($value)) {
                                $compiledMessage .= implode(', ', $value);
                            } else {
                                $compiledMessage .= $value;
                            }
                        }
                    }
                    
                    if (!empty($postedMessage['body'])) {
                        if ($compiledMessage) {
                            $compiledMessage .= "\n\n";
                        }
                        
                        $compiledMessage .= $postedMessage['body'];
                    }
                    
                    $message->message = $compiledMessage;
                }
            } else {
                $message->message = $postedMessage;
                $message->messageFields = array('body' => $postedMessage);
            }
        }
        
        if (empty($message->htmlMessage)) {
            $message->htmlMessage = StringHelper::parseMarkdown($message->message);
        }
        
        if ($message->validate()) {
            // Only actually send it if the honeypot test was valid
            if (!$this->validateHoneypot($settings->honeypotField) || craft()->contactForm->sendMessage($message)) {
                if (craft()->request->isAjaxRequest()) {
                    $this->returnJson(array('success' => true));
                } else {
                    // Deprecated. Use 'redirect' instead.
                    $successRedirectUrl = craft()->request->getPost('successRedirectUrl');
                    
                    if ($successRedirectUrl) {
                        $_POST['redirect'] = $successRedirectUrl;
                    }
                    
                    craft()->userSession->setNotice($settings->successFlashMessage);
                    $this->redirectToPostedUrl($message);
                }
            }
        }
        
        // Something has gone horribly wrong.
        if (craft()->request->isAjaxRequest()) {
            return $this->returnErrorJson($message->getErrors());
        } else {
            craft()->userSession->setError('There was a problem with your submission, please check the form and try again!');
            
            if ($savedBody !== false) {
                $message->message = $savedBody;
            }
            
            craft()->urlManager->setRouteVariables(array(
              'message' => $message
            ));
        }
    }
    
    /**
     * Checks that the 'honeypot' field has not been filled out (assuming one has been set).
     *
     * @param string $fieldName The honeypot field name.
     * @return bool
     */
    protected function validateHoneypot($fieldName)
    {
        if (!$fieldName) {
            return true;
        }
        
        $honey = craft()->request->getPost($fieldName);
        return $honey == '';
    }
}
