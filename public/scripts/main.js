// import 'bootstrap-sass/assets/javascripts/bootstrap/modal'; // FIXME: Remove if not needed.
/**
 * NOTE: We require the main sass stylesheet here so that extract-text-webpack-plugin can
 * then take this module and create a separate stylesheet for it. If we remove that plugin
 * then webpack will automatically inject the styles at the top of the page. With this we
 * have control of the link tag's placement in `_layout.html`.
 */
import 'slick-carousel';
import '../styles/main.scss';

$(() => {
  $('[action="toggle-menu"]').click(() => {
    $('nav').toggleClass('menu');
    $('nav, [action="toggle-menu"]').toggleClass('active');
    $('#mainNav, .social-btn, .contacts').toggleClass('hidden');
  });
  $('[action="toggle-search"]').click(() => {
    $('nav').toggleClass('search');
    $('nav, [action="toggle-search"]').toggleClass('active');
    $('.searching').toggleClass('hidden');
  });
  $('.caseStudies, .testmonial-slider').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    prevArrow: '<div class="slick-prev"><svg width="19px" height="70px" viewBox="0 0 19 70" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Home" transform="translate(-1371.000000, -4123.000000)" stroke="#1081C8"><polyline id="Triangle" points="1372 4123 1389 4158 1372 4193"></polyline></g></g></svg></div>',
    nextArrow: '<div class="slick-next"><svg width="19px" height="70px" viewBox="0 0 19 70" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Home" transform="translate(-1371.000000, -4123.000000)" stroke="#1081C8"><polyline id="Triangle" points="1372 4123 1389 4158 1372 4193"></polyline></g></g></svg></div>',
    responsive: [
      {
        breakpoint: 600,
        settings: {
          adaptiveHeight: true,
          arrows: false,
          dots: true
        }
      }
    ]
  });
  $('.blog-slide').slick({
    dots: true,
    infinite: true,
    arrows: false,
    slidesToShow: 1
  });
  $('.read-more').click((e) => {
    e.preventDefault();
    $(e.target).parent().toggleClass('active');
  });
  $(() => {
    const url = window.location.href;
    $('.nav-menu a').each(function() {
      if (url === (this.href)) {
        $('.nav-menu li').removeClass('active');
        $(this).parent('li').addClass('active');
      }
    });
  });
  $('.edit-link').click(function(e) {
    e.preventDefault();
    window.open(this.href, 'mywindow', 'width=400, height=200');
  });
  $('.carousel').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true
        }
      }
    ]
  });
  const blogUrl = `${window.location.origin}/blog`;
  if (document.URL === blogUrl) {
    window.addEventListener('load', () => {
      setTimeout(() => {
        window.grid = new Masonry('.grid'); // eslint-disable-line no-new, prefer-const, no-unused-vars, no-undef
      }, 2000);
    });
    $(() => {
      window.grid = new Masonry('.grid', {}); // eslint-disable-line no-new, prefer-const, no-unused-vars, no-undef
      let page = 2;
      $('.loadMore').click((e) => {
        e.preventDefault();
        let oldhtml = $('.grid').append('<div hidden id="df"></div>'); // eslint-disable-line prefer-const, no-unused-vars
        $('#df').load(`/blog/p${page} .grid`, () => {
          $('#grid').append($('#df .grid').html());
          $('#df').remove();
          page++;
          $('img').bind('load', () => {
            new Masonry('.grid', {}); // eslint-disable-line no-new, no-undef
          });
        });
      });
    });
  }
  $('.blog-container').parents().addClass('single-blog');
  $('.change-bg').parents().addClass('nav-color');
  window.addEventListener('load', () => {
    function equalHeight(group) {
      let tallest = 0;
      group.each(function() {
        const thisHeight = $(this).height();
        if (thisHeight > tallest) {
          tallest = thisHeight;
        }
      });
      group.height(tallest);
    }
    equalHeight($('.equal'));
    equalHeight($('.entry-title'));
  });

  $('form.blog-filter').submit((e) => {
    e.preventDefault();
  });

  window._myModel = function() {
    return {
      cat: $('select[name="category"] option:selected').val(),
      year: $('select[name="year"] option:selected').val(),
      q: $('#filter-search').val()
    };
  };

  const noContent = '<h2 class="text-center">Awww, there are no entries for this search.</h2>';

  $('select[name="category"]').change(() => {
    const categ = window._myModel();
    $('#grid').load($('.blog-filter').data('action') + `?year=${categ.year}&category=${categ.cat}&q=${categ.q} .grid-item`, () => { // eslint-disable-line prefer-template
      $('img').bind('load', () => {
        window.grid = new Masonry('.grid', {});  // eslint-disable-line no-new, prefer-const, no-unused-vars, no-undef
      });
    });
    $('.next-page').remove();
    setTimeout(() => {
      if ($('#grid').is(':empty')) {
        $('#grid').css('height', 'auto').append(noContent);
      }
    }, 1000);
  });

  $('[name="year"]').change(() => {
    const years = window._myModel();
    $('#grid').load($('.blog-filter').data('action') + `?year=${years.year}&category=${years.cat}&q=${years.q} .grid-item`, () => { // eslint-disable-line prefer-template
      $('img').bind('load', () => {
        window.grid = new Masonry('.grid', {});  // eslint-disable-line no-new, prefer-const, no-unused-vars, no-undef
      });
    });
    $('.next-page').remove();
    setTimeout(() => {
      if ($('#grid').is(':empty')) {
        $('#grid').css('height', 'auto').append(noContent);
      }
    }, 1000);
  });

  $('#filter-search').change(() => {
    const searche = window._myModel();
    $('#grid').load($('.blog-filter').data('action') + `?year=${searche.year}&category=${searche.cat}&q=${searche.q} .grid-item`, () => { // eslint-disable-line prefer-template
      $('img').bind('load', () => {
        window.grid = new Masonry('.grid', {});  // eslint-disable-line no-new, prefer-const, no-unused-vars, no-undef
      });
    });
    $('.next-page').remove();
    setTimeout(() => {
      if ($('#grid').is(':empty')) {
        $('#grid').css('height', 'auto').append(noContent);
      }
    }, 1000);
  });

  window.addEventListener('load', () => {
    const URL = window.location.href;
    const blog = window.location.origin;
    if (URL.indexOf('?') !== -1) {
      const query = URL.split('?');
      $('#backToBlog').attr('href', `${blog}/blog?${query[1]}`);
      const selected = query[1].split('&');
      const year = selected[0].split('=');
      const cat = selected[1].split('=');
      const q = selected[2].split('=');
      $(`select[name="year"] option[value="${year[1]}"]`).attr('selected', 'selected');
      $(`select[name="category"] option[value="${cat[1]}"]`).attr('selected', 'selected');
      $('#filter-search').val(q[1].replace(/[^\w\s]/gi, ''));
      $('#grid').load($('.blog-filter').data('action') + `?${query[1]} .grid-item`, () => { // eslint-disable-line prefer-template
        $('img').bind('load', () => {
          window.grid = new Masonry('.grid', {});  // eslint-disable-line no-new, prefer-const, no-unused-vars, no-undef
        });
      });
    }
  }, false);

  const responsiveViewport = $(window).width();
  if (responsiveViewport < 481) {
    $('.text-right, article, .mgn-right').removeClass('text-right pdn-right mgn-right');
    $('.slick-arrow').remove();
    $('.key-services section').css('background-image', 'none');
  }

  (function() {
    $(window).scroll(function() {
      const currentScroll = $(this).scrollTop();
      const elementOffset = $('.forNavSection').offset().top;
      const distance = (elementOffset - currentScroll);
      $('body > nav').addClass('nav-fixed').css('background', 'rgba(0, 0, 0, 0.6)');
      if (distance >= 66) {
        $('.nav-fixed').css('background', 'transparent');
      } else {
        $('body > nav').addClass('nav-fixed').css('background', 'rgba(0, 0, 0, 0.6)');
      }
    });
  }());
  if (responsiveViewport < 768) {
    $('thoughts img').css('width', '100%');
  }
  if (responsiveViewport >= 768) {
    const setTop = function(number) {
      const subHead = document.getElementById(`movements_${number}`);
      const topHeight = document.getElementById(`subHead_${number}`);
      subHead.style.top = linearScale( // eslint-disable-line
        window.pageYOffset,
        [
          topHeight.offsetTop - subHead.offsetHeight - (window.innerHeight / 2),
          topHeight.offsetTop - (window.innerHeight / 2)
        ],
        [
          topHeight.offsetHeight,
          -60
        ]
      ) + 'px';
    };
    window.onscroll = function(e) {
      e.preventDefault();
      setTop('1');
      setTop('2');
      setTop('3');
      setTop('4');
      setTop('5');
      setTop('6');
      setTop('7');
      setTop('8');
      setTop('9');
    };
    window.onerror = function(message, url, lineNumber) { // eslint-disable-line no-unused-vars
      return true;
    };
  }

  function linearScale(a, b, c) {
    b[0] < b[1] ? (a < b[0] && (a = b[0]), a > b[1] && (a = b[1])) : (a > b[0] && (a = b[0]), a < b[1] && (a = b[1]));  // eslint-disable-line
    a = (a - b[0]) * (c[1] - c[0]) / (b[1] - b[0]) + c[0];
    return a = c[1] > c[0] ? a > c[1] ? c[1] : a : a > c[0] ? c[0] : a  // eslint-disable-line
  }
  $('.autoplay').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true
        }
      }
    ]
  });
  $('.awards-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});
